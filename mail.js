const nodemailer = require('nodemailer');
const serial = require('uuidv4').default;

const transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
});

const makeAnEmail = text => `
    <div className='email' style="
        border: 1px solid black;
        padding: 20px;
        line-height: 2;
        font-size: 20px;
        text-align: center;
    ">
        <h2 style="color: #004579">Appointment Confirmation</h2>
        <p>Your appointment has been confirmed. Please check the details</p>
        <div>${text}</div>
        <h4>Appointment Serial: ${serial().substring(0, 4)}</h4>
        <div>Please don't forget to bring your Appointment Serial.</div>
    </div>
`;

const makeEmailForDoc = text => `
<div className='email' style="
        border: 1px solid black;
        text-align: center;
        line-height: 2;
        font-size: 20px;
        
    ">
        <h2 style="color: #004579">Patient Report</h2>
       
        <div>${text}</div>
        
    </div>

`;

exports.transport = transport;
exports.makeAnEmail = makeAnEmail;
exports.makeEmailForDoc = makeEmailForDoc;
