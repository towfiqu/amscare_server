const express = require('express');
const cors = require('cors');
const doctors = require('../routes/doctors');
const facilities = require('../routes/facilities');
const patients = require('../routes/patients');
const patient_register = require('../routes/patient_register');
const appointments = require('../routes/appointments');
const cabins = require('../routes/cabins');
const medical_tests = require('../routes/medical_tests');

module.exports = function(app) {
  // express middleware
  app.use(cors());
  app.use(express.json());

  // Route kick off points
  app.use('/api/doctors', doctors);
  app.use('/api/facilities', facilities);
  app.use('/api/patients', patients);
  app.use('/api/patients', patient_register);
  app.use('/api/appointments', appointments);
  app.use('/api/cabins', cabins);
  app.use('/api/medical_tests', medical_tests);
};
