module.exports = function() {
  let db;
  if (process.env.NODE_ENV === 'dev') {
    db = 'mongodb://localhost/ams_db';
    return db;
  }
  if (process.env.NODE_ENV === 'prod') {
    db = process.env.DB_STRING;
    return db;
  }
};
