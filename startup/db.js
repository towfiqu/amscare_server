const mongoose = require('mongoose');
const db = require('./config')();

// MongoDb Database initiation
module.exports = async function() {
  try {
    await mongoose.connect(db, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log('Connected to ams_db ...');
    console.log(db);
  } catch (err) {
    console.error('Could not connect to ams_db !!!', err);
  }
};
