// main app.js this is the file that will run at the start of the server.
const express = require('express');
const app = express();

// all the necessary requirements needed for the site to run.
// environment variables, database configuration, initialization of all routes.
require('dotenv').config({ path: 'variables.env' });
require('./startup/db')();
require('./startup/routes')(app);

// setup for running the site on local server.
const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server started on port ${port} ...`);
});
