const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const { Patient } = require('../models/patient');
const { Patient_register } = require('../models/patient_register');
const { transport, makeEmailForDoc } = require('../mail');

// create profile route
router.post('/create_profile/:id', auth, async (req, res) => {
  try {
    const patient = await Patient_register.findById(req.params.id);

    const profile = new Patient({
      patient_register_id: patient._id,
      patient_email: patient.email,
      patient_name: req.body.name,
      patient_phone: req.body.phone,
      patient_address: req.body.address,
      patient_pic: req.body.pic,
      patient_reports: req.body.reports,
    });
    await profile.save();
    res.send(profile);
  } catch (err) {
    console.error('Ooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

//update profile
router.put('/update_profile/:id', auth, async (req, res) => {
  try {
    const patient = await Patient.findOne({
      patient_register_id: req.params.id,
    });

    const profile = await Patient.findByIdAndUpdate(patient._id, {
      patient_name: req.body.name,
      patient_phone: req.body.phone,
      patient_address: req.body.address,
      patient_pic: req.body.pic,
      new: true,
    });
    if (!profile) return res.status(404).send('Profile Not found');
    await profile.save();
    res.send(profile);
  } catch (err) {
    console.error('Ooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

// get profile
router.get('/get_profile/:id', auth, async (req, res) => {
  try {
    const profile = await Patient.findOne({
      patient_register_id: req.params.id,
    });

    if (!profile) return res.status(400).send('Profile is yet to be created');
    else {
      res.send(profile);
    }
  } catch (err) {
    console.error('Ooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

// profile delete
router.delete('/delete_profile/:id', auth, async (req, res) => {
  try {
    const patient = await Patient.findOne({
      patient_register_id: req.params.id,
    });
    const find_patient = await Patient.findByIdAndRemove(patient._id);
    // res.send(find_patient);
    await Patient_register.findByIdAndRemove(find_patient.patient_register_id);
    res.send('Profile removed');
  } catch (err) {
    console.error('Ooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

router.post('/add_report/:id', auth, async (req, res) => {
  try {
    const profile = await Patient.findOne({
      patient_register_id: req.params.id,
    });

    let reports = profile.patient_reports;

    reports = reports.concat(req.body.report);

    const update_profile = await Patient.findByIdAndUpdate(profile._id, {
      patient_reports: reports,
      new: true,
    });
    await update_profile.save();
    res.send(update_profile.patient_reports);
  } catch (err) {
    console.error('OOOPS!!!', err);
    return res.status(400).send('Something went wrong!!!');
  }
});

router.post('/delete_report/:id', auth, async (req, res) => {
  try {
    const profile = await Patient.findOne({
      patient_register_id: req.params.id,
    });

    let reports = profile.patient_reports;

    const index = reports.indexOf(req.body.image);
    reports.splice(index, 1);

    const updatedReports = await Patient.findByIdAndUpdate(profile._id, {
      patient_reports: reports,
      new: true,
    });
    await updatedReports.save();
    res.send(updatedReports);
  } catch (err) {
    console.error('OOOPS!!!', err);
    return res.status(400).send('Something went wrong!!!');
  }
});

// Send report to doctor
router.post('/send_report', async (req, res) => {
  try {
    const data = {
      doc_email: req.body.doc_email,
      patient_email: req.body.patient_email,
      patient_name: req.body.patient_name,
      patient_phone: req.body.patient_phone,
      report: req.body.report,
    };

    await transport.sendMail({
      from: data.patient_email,
      to: data.doc_email,
      subject: 'Patient Medical Report',
      html: makeEmailForDoc(
        ` 
          <h3 style="margin: 0;">Name: ${data.patient_name}</h3>
          <p style="margin: 0;">Email: ${data.patient_email}</p>
          <p style="margin: 0;">Phone: ${data.patient_phone}</p>
          <img style="text-align: center; margin-top: 10px;" src=${data.report}>
         
          `,
      ),
    });
    return res.status(200).json({ msg: 'report sent' });
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

module.exports = router;
