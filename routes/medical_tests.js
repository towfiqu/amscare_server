const express = require('express');
const router = express.Router();
const { MedicalTest } = require('../models/medical_test');

router.post('/add_test', async (req, res) => {
  try {
    const test = new MedicalTest({
      test_name: req.body.name,
      test_cost: req.body.cost,
      facility: req.body.facility,
      location: req.body.location,
      facility_id: req.body.facility_id
    });
    await test.save();
    res.send(test);
  } catch (err) {
    console.error('Oooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/getAll', async (req, res) => {
  try {
    const tests = await MedicalTest.find();
    res.send(tests);
  } catch (err) {
    console.error('Oooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_tests_by_name/:name', async (req, res) => {
  try {
    const test_name = new RegExp(`.*${req.params.name}.*`, 'i');
    const tests = await MedicalTest.find({ test_name: test_name });
    res.send(tests);
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/getTotalTests', async (req, res) => {
  try {
    const total = await MedicalTest.estimatedDocumentCount();
    res.send(total.toString());
  } catch (err) {
    console.error('Oooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});

module.exports = router;
