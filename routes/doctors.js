const express = require('express');
const router = express.Router();
const { Doctor } = require('../models/doctor');

router.post('/add_doctor', async (req, res) => {
  try {
    const doctor = new Doctor({
      doctor_name: req.body.name,
      doctor_speciality: req.body.speciality,
      doctor_bio: req.body.bio,
      doctor_email: req.body.email,
      doctor_phone: req.body.phone,
      doctor_image: req.body.image,
      visiting_fees: req.body.fees,
      business_hours: req.body.hours,
      facility_id: req.body.facility_id
    });
    await doctor.save();
    res.send(doctor);
  } catch (err) {
    console.error('Ooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_doctors_by_facility/:id', async (req, res) => {
  try {
    const doctor = await Doctor.find({ facility_id: req.params.id });

    res.send(doctor);
  } catch (err) {
    console.error('OoooPs!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_doctors_by_speciality/:speciality', async (req, res) => {
  try {
    const doctor = await Doctor.find({
      doctor_speciality: req.params.speciality
    });
    res.send(doctor);
  } catch (err) {
    console.error('Oooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_doctors_by_name/:name', async (req, res) => {
  try {
    const name = new RegExp(`.*${req.params.name}.*`, 'i');
    const doctor = await Doctor.find({ doctor_name: name });
    res.send(doctor);
  } catch (err) {
    console.error('Ooooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

router.get('/get_doctor_by_id/:id', async (req, res) => {
  try {
    const doctor = await Doctor.find({ _id: req.params.id });
    res.send(doctor);
  } catch (err) {
    console.error('Ooooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

router.put('/update_doctor/:id', async (req, res) => {
  try {
    const doctor = await Doctor.findByIdAndUpdate(req.params.id, {
      doctor_name: req.body.name,
      doctor_speciality: req.body.speciality,
      doctor_bio: req.body.bio,
      doctor_email: req.body.email,
      doctor_phone: req.body.phone,
      doctor_image: req.body.image,
      visiting_fees: req.body.fees,
      business_hours: req.body.hours,
      facility_id: req.body.facility_id,
      new: true
    });
    await doctor.save();
    res.send(doctor);
  } catch (err) {
    console.error('Oooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_specialities', async (req, res) => {
  try {
    const specialities = await Doctor.find().distinct('doctor_speciality');
    res.send(specialities);
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.post('/get_doctors_by_facility_and_speciality', async (req, res) => {
  try {
    const doctors = await Doctor.find({
      facility_id: req.body.facility,
      doctor_speciality: req.body.speciality
    });
    res.send(doctors);
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.post(
  '/get_doctors_by_facility_speciality_and_name',
  async (req, res) => {
    try {
      const name = new RegExp(`.*${req.body.name}.*`, 'i')
      const doctors = await Doctor.find({
        facility_id: req.body.facility,
        doctor_speciality: req.body.speciality,
        doctor_name: name
      });
      res.send(doctors);
    } catch (err) {
      console.error('Ooops!!', err);
      return res.status(400).send('Something went wrong!!');
    }
  }
);
router.get('/getTotalDoctors', async (req, res) => {
  try {
    const total = await Doctor.estimatedDocumentCount();
    res.send(total.toString());
  } catch (err) {
    console.error('Oooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});
router.get('/getAll', async (req, res) => {
  try {
    const all = await Doctor.find();
    res.send(all);
  } catch (err) {
    console.error('Oooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});

module.exports = router;
