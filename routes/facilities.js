const express = require('express');
const router = express.Router();
const { Facility } = require('../models/facility');

router.post('/add_facility', async (req, res) => {
  try {
    const facility = new Facility(req.body);
    await facility.save();
    res.send(facility);
  } catch (err) {
    console.error('Oooops!!!', err);
  }
});
router.get('/get_facility_by_location/:location', async (req, res) => {
  try {
    const facility = await Facility.find({
      facility_address: req.params.location,
    });
    console.log(facility);
    res.send(facility);
  } catch (err) {
    console.error('Oooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.put('/update_facility/:id', async (req, res) => {
  try {
    const facility = await Facility.findByIdAndUpdate(req.params.id, {
      facility_name: req.body.name,
      facility_email: req.body.email,
      facility_phone: req.body.phone,
      facility_address: req.body.address,
      facility_image: req.body.image,
      cabin_capacity: req.body.cabin,
      new: true,
    });
    await facility.save();
    res.send(facility);
  } catch (err) {
    console.error('Oooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_names', async (req, res) => {
  try {
    const names = await Facility.find().select('facility_name');
    res.send(names);
  } catch (err) {
    console.error('Oooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_facility_by_id/:id', async (req, res) => {
  try {
    const facility = await Facility.findOne({
      _id: req.params.id,
    });
    res.send(facility);
  } catch (err) {
    console.error('Ooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});
router.get('/get_facility_id_by_name/:name', async (req, res) => {
  try {
    const facility_id = await Facility.findOne({
      facility_name: req.params.name,
    }).select('_id');
    res.send(facility_id);
  } catch (err) {
    console.error('Ooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});
router.get('/getTotalFacilities', async (req, res) => {
  try {
    const total = await Facility.estimatedDocumentCount();
    res.send(total.toString());
  } catch (err) {
    console.error('Oooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});
router.get('/getAll', async (req, res) => {
  try {
    const all = await Facility.find();
    res.send(all);
  } catch (err) {
    console.error('Oooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});

router.get('/find_nearby_facilities', async (req, res) => {
  try {
    const coordinates = [req.query.lng, req.query.lat].map(parseFloat);
    // res.json(coords);
    const query = {
      location: {
        $near: {
          $geometry: {
            type: 'Point',
            coordinates,
          },
          $maxDistance: 10000,
        },
      },
    };

    const facilities = await Facility.find(query);
    res.send(facilities);
  } catch (err) {
    console.error(err);
    res.status(400).send('Something Went Wrong');
  }
});

module.exports = router;
