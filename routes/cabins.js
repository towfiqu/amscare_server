const express = require('express');
const stripe = require('stripe')(process.env.STRIPE_SECRET);
const router = express.Router();
const { Cabin } = require('../models/cabin');
const { Booked_cabin } = require('../models/booked_cabin');
const { Facility } = require('../models/facility');

router.post('/payment', (req, res) => {
  const body = {
    source: req.body.token.id,
    amount: req.body.amount,
    currency: 'usd',
  };
  stripe.charges.create(body, (stripeErr, stripeRes) => {
    if (stripeErr) {
      res.status(500).send({ error: stripeErr });
    } else {
      res.status(200).send({ success: stripeRes });
    }
  });
});

router.post('/add_cabin', async (req, res) => {
  try {
    const cabin = new Cabin({
      cabin_number: req.body.number,
      cabin_image: req.body.image,
      cabin_floor: req.body.floor,
      cabin_beds: req.body.beds,
      cabin_AC: req.body.ac,
      cabin_cost: req.body.cost,
      facility_id: req.body.facility_id,
      cabin_booked: req.body.booked,
    });

    await cabin.save();

    // update cabin capacity after adding one
    const facility = await Facility.findById(req.body.facility_id);
    facility.cabin_capacity++;
    facility.save();

    res.send(cabin);
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

router.post('/book_cabin', async (req, res) => {
  try {
    const book_cabin = new Booked_cabin({
      patient_name: req.body.patient_name,
      patient_email: req.body.patient_email,
      patient_phone: req.body.patient_phone,
      facility: req.body.facility,
      cabin_number: req.body.cabin_number,
      cabin_cost: req.body.cabin_cost,
      check_in: req.body.check_in,
      cabin_id: req.body.cabin_id,
      facility_id: req.body.facility_id,
    });
    await book_cabin.save();

    // update cabin capacity in facility
    const facility = await Facility.findById(req.body.facility_id);
    facility.cabin_capacity--;
    facility.save();

    // update cabin availability in Cabin
    const cabin = await Cabin.findById(req.body.cabin_id);
    cabin.cabin_booked = true;
    cabin.save();

    res.send(book_cabin);
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

router.get('/get_booked_cabin_by_patient/:id', async (req, res) => {
  try {
    const cabin = await Booked_cabin.find({ patient_id: req.params.id });
    res.send(cabin);
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/getAll', async (req, res) => {
  try {
    const all = await Cabin.find();
    res.send(all);
  } catch (err) {
    console.error('Oooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});
router.get('/getTotalCabins', async (req, res) => {
  try {
    const total = await Cabin.estimatedDocumentCount();
    res.send(total.toString());
  } catch (err) {
    console.error('Oooops!!', err);
    res.status(400).send('Something went wrong!');
  }
});
router.get('/get_cabins_by_facility/:id', async (req, res) => {
  try {
    const cabins = await Cabin.find({ facility_id: req.params.id });
    res.send(cabins);
  } catch (err) {
    console.error('OOOPS!!', err);
    res.status(400).send('Something went wrong!');
  }
});
router.get('/get_single_cabin/:id', async (req, res) => {
  try {
    const cabin = await Cabin.findOne({ _id: req.params.id });
    res.send(cabin);
  } catch (err) {
    console.error('OOPS!!', err);
    res.status(400).send('Something went wrong!');
  }
});

module.exports = router;
