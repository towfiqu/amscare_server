const express = require('express');
const router = express.Router();
const { Appointment } = require('../models/appointment');
const { transport, makeAnEmail } = require('../mail');

router.post('/add_appointment', async (req, res) => {
  try {
    const appointment = new Appointment({
      patient_name: req.body.name,
      patient_email: req.body.email,
      patient_phone: req.body.phone,
      appointment_date: req.body.date,
      doctor: req.body.doctor,
      speciality: req.body.speciality,
      facility: req.body.facility,
      location: req.body.location
    });
    await appointment.save();
    res.json(appointment);

    const apt_res = appointment;
    await transport.sendMail({
      from: 'ams@mail.com',
      to: apt_res.patient_email,
      subject: 'Appointment confirmation email',
      html: makeAnEmail(
        `<h3>Appointment Details</h3> 
          <div>Name:${apt_res.patient_name}</div> 
          <div>Doctor: ${apt_res.doctor} </div>
          <div>Appointment Date: <strong>${apt_res.appointment_date}</strong></div>
          <div>facility: ${apt_res.facility}</div>`
      )
    });
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});
router.get('/get_appointment_by_patient/:id', async (req, res) => {
  try {
    const appointment = await Appointment.find({ patient_id: req.params.id });
    res.send(appointment);
  } catch (err) {
    console.error('Ooops!!', err);
    return res.status(404).send('Not found!!');
  }
});

module.exports = router;
