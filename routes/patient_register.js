const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();
const auth = require('../middleware/auth');
const { Patient_register } = require('../models/patient_register');

router.post('/register', async (req, res) => {
  try {
    const patient_email = await Patient_register.findOne({
      email: req.body.email
    });
    if (patient_email)
      return res.status(400).json({ msg: 'email already registered!' });
    const patient = new Patient_register({
      email: req.body.email,
      password: req.body.password
    });
    const salt = await bcrypt.genSalt(10);
    patient.password = await bcrypt.hash(patient.password, salt);
    await patient.save();
    res.send(patient);
  } catch (err) {
    console.error('Ooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

router.post('/login', async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  try {
    const patient = await Patient_register.findOne({ email: email });

    if (!patient) {
      return res.status(400).json({ msg: 'Inavlid Email or Password!' });
    }

    const validPass = await bcrypt.compare(password, patient.password);
    if (!validPass)
      return res.status(400).json({ msg: 'Inavlid Email or Password!' });
    const token = patient.generateAuthToken();
    res.header('x-auth-token', token);
    res.send(token);
  } catch (err) {
    console.error('Ooops!!!', err);
    return res.status(400).send('Something went wrong!!');
  }
});

router.get('/auth', auth, async (req, res) => {
  const patient = await Patient_register.findById(req.payload.id).select(
    '-password'
  );

  res.send(patient);
});

module.exports = router;
