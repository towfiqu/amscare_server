const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const schema = mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

schema.methods.generateAuthToken = function() {
  const token = jwt.sign({ id: this._id }, process.env.jwtKey);
  return token;
};

const Patient_register = mongoose.model('Patient_register', schema);

module.exports.Patient_register = Patient_register;
