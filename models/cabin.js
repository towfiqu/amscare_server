const mongoose = require('mongoose');

const schema = mongoose.Schema({
  cabin_number: {
    type: String,
    required: true,
  },
  cabin_image: {
    type: String,
    required: true,
  },
  cabin_floor: {
    type: String,
    required: true,
  },
  cabin_beds: {
    type: Number,
    required: true,
  },
  cabin_AC: {
    type: String,
    default: 'No',
  },
  cabin_cost: {
    type: Number,
    rqeuired: true,
  },
  facility_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'facilities',
  },
  cabin_booked: {
    type: Boolean,
    default: false,
  },
});

const Cabin = mongoose.model('Cabin', schema);

module.exports.Cabin = Cabin;
