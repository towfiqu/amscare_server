const mongoose = require('mongoose');

const schema = mongoose.Schema({
  patient_name: {
    type: String,
    required: true
  },
  patient_email: {
    type: String,
    required: true
  },
  patient_phone: {
    type: String,
    required: true
  },
  appointment_date: {
    type: String,
    required: true
  },
  doctor: {
    type: String,
    required: true
  },
  speciality: {
    type: Array,
    required: true
  },
  facility: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  }
});

const Appointment = mongoose.model('Appointment', schema);

module.exports.Appointment = Appointment;
