const mongoose = require('mongoose');

const schema = mongoose.Schema({
  doctor_name: {
    type: String,
    required: true
  },
  doctor_speciality: {
    type: Array,
    required: true
  },
  doctor_bio: {
    type: String,
    required: true
  },
  doctor_email: {
    type: String,
    required: true
  },
  doctor_phone: {
    type: String,
    required: true
  },
  doctor_image: {
    type: String,
    required: true
  },
  visiting_fees: {
    type: Number,
    required: true
  },
  business_hours: {
    type: Array,
    required: true
  },
  facility_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'facilities'
  }
});

const Doctor = mongoose.model('Doctor', schema);

module.exports.Doctor = Doctor;
