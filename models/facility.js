const mongoose = require('mongoose');

const schema = mongoose.Schema({
  facility_name: {
    type: String,
    required: true
  },
  facility_email: {
    type: String,
    required: true
  },
  facility_phone: {
    type: String,
    required: true
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number,
      required: true
    }],
    facility_address: {
      type: String,
      required: true
    }
  },
  facility_image: {
    type: String,
    required: true
  },
  cabin_capacity: {
    type: Number,
    default: 0
  }
});

const Facility = mongoose.model('Facility', schema);

module.exports.Facility = Facility;
