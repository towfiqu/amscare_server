const mongoose = require('mongoose');

const schema = mongoose.Schema({
  test_name: {
    type: String,
    required: true
  },
  test_cost: {
    type: Number,
    required: true,
    default: 0
  },
  facility: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  facility_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'facilities'
  }
});

const MedicalTest = mongoose.model('Medical_test', schema);

module.exports.MedicalTest = MedicalTest;
