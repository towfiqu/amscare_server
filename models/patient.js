const mongoose = require('mongoose');

const schema = mongoose.Schema({
  patient_register_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Patient_register'
  },
  patient_name: {
    type: String,
    required: true,
    minlength: 3
  },
  patient_email: {
    type: String,
    required: true,
    unique: true
  },
  patient_phone: {
    type: String,
    required: true
  },
  patient_pic: {
    type: String,
    required: true
  },
  patient_address: {
    type: String,
    required: true
  },
  patient_reports: {
    type: Array
  }
});

const Patient = mongoose.model('Patient', schema);

module.exports.Patient = Patient;
