const mongoose = require('mongoose');

const schema = mongoose.Schema({
  patient_name: {
    type: String,
    required: true
  },
  patient_email: {
    type: String,
    required: true
  },
  patient_phone: {
    type: String,
    required: true
  },
  facility: {
    type: String,
    required: true
  },
  cabin_number: {
    type: String,
    required: true
  },
  cabin_cost: {
    type: Number,
    required: true
  },
  check_in: {
    type: Date,
    default: Date.now()
  },
  cabin_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'cabins'
  },
  facility_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'facilities'
  }
});

const Booked_cabin = mongoose.model('Booked_cabin', schema);

module.exports.Booked_cabin = Booked_cabin;
