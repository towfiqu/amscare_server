const jwt = require('jsonwebtoken');

// Authentication middleware to protect private routes.
// Without valid token access to a protected route will be blocked

module.exports = function(req, res, next) {
  const token = req.header('x-auth-token');
  if (!token) return res.status(401).send('Access denied. No token Provided');

  try {
    const decoded = jwt.verify(token, process.env.jwtKey);
    req.payload = decoded;
    next();
  } catch (err) {
    res.status(400).send('Invalid token', err);
  }
};
